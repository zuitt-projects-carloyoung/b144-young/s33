// JSON web token are standard for sending info betwwen our app in a secure manner
// will allow us to gain access to methods that will help us to creat a JSON web token

const jwt = require('jsonwebtoken');
const secret = "CrushAkongCrushKo";

// JWT is a way of securely passing info from the server to the other parts of the server
// info is kept secure through the use of the secret code
// only the sytem that knows the secret code that can decode the encrypted info

// Token Creation
// Analogy: Pack the gift and provide a lock with the secret code as the key

module.exports.createAccessToken = (user) => {
	// Data will be received from the registration form
	// when the users log in, a token will be created with user's information
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	};
	// generates the token using the form data and the secret code with no additional options provided
	return jwt.sign(data, secret, {})
}
// ACTIVITY
// module.exports.createCourseToken = (course) => {
// 	const data2 = {
// 		name: course.name,
// 		desrciption: course.desrciption,
// 		price: course.price
// 	};
// 	return jwt.sign(data2, secret, {})
// }


// Token Verification
module.exports.verify = (req, res, next) => {
	
	let token = req.headers.authorization;

	if(typeof token !== "undefined"){
		console.log(token);
		// Bearer
		token = token.slice(7, token.length)

		// Validate the token using the "verify" method
		return jwt.verify(token, secret, (err, data) => {
			// if JWT is not valid
		if(err){
			return res.send({auth:"failed"});
		}
		else{
			// Allows the application to proceed with the next middleware function/callback function in the route
			next();

		}
		})
	}
	// token does not exist
	else{
		return res.send({auth:"failed"});
	}
} 

// Token decryption

module.exports.decode = (token) => {
	// Token received and is not undefined
	if(typeof token !== "undefined"){
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err,data) => {
			if(err){
				return null;
			}
			else{
				// "decode mthod is used to obtain the information from the JWT"
				// "{complete: true}" option allows to return additional information from the JWT
				// returns an object with access to the "payload" property which contains uder information stored when the token was generated.
				return jwt.decode(token, {complete: true}).payload;
			}
		})
	}
	else{
		return null;	
	}
}
